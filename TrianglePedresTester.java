package recursividad;

class TrianglePedres {
	int[] resultat = { 0, 0 };

	int[] calcularFiles(int restants, int necessaries) {
		System.out.println(restants + ", " + restants);
		if (restants < necessaries) {
			resultat[0] = necessaries - 1;
			resultat[1] = restants;
			return resultat;
		} else {
			return calcularFiles(restants - necessaries, necessaries + 1);
		}
	}
}

public class TrianglePedresTester {
	public static void main(String[] args) {
		TrianglePedres test = new TrianglePedres();
		int[] resultat = test.calcularFiles(13, 1);
		System.out.println("restants: "+ resultat[0]);
		System.out.println("necessaries: "+ resultat[1]);
	}
}
